﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace Collatz
{
    [TestClass]
    public class CollatzTest
    {
        [TestMethod]
        public void CollatzFrom1_ShouldComesToOne()
        {
            int ausgabgsZahl = 1;
            bool result = IsCollatzComesToOne(ausgabgsZahl);
            result.Should().BeTrue();
        }

        [TestMethod]
        public void CollatzFrom99_ShouldComesToOne()
        {
            int ausgabgsZahl = 99;
            bool result = IsCollatzComesToOne(ausgabgsZahl);
            result.Should().BeTrue();
        }

        [TestMethod]
        public void CollatzFrom1Til99_ShouldComesToOne()
        {
            for(int i = 1; i < 100; ++i)
            {
                IsCollatzComesToOne(i).Should().BeTrue();
            }
        }

        private bool IsCollatzComesToOne(int zahl)
        {
            if(zahl != 1)
            {
                if (zahl % 2 == 0)
                {
                    return IsCollatzComesToOne((zahl / 2));
                }
                else
                {
                    return IsCollatzComesToOne((zahl * 3 + 1));
                }
            }
            return true;
            
        }
    }
}
